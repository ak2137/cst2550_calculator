import org.junit.*;
import static org.junit.Assert.*;

public class CalculatorTest{

    @Test
    public void testAdd(){
	double n1 = 2.5;
	double n2 = 0;
	double expected = 2.5;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testSubtract(){
	double expectedResult = 1;
	assertEquals(expectedResult, Calculator.subtract(2, 1), 1e-6);
    }

    @Test
    public void testMultiply(){
	double n1 = 2;
	double n2 = 5;
	double expected = 10;
	double result = Calculator.multiply(n1, n2);
	assertEquals(expected, result, 1e-6);}

     @Test
    public void testDivide(){
	assertEquals(5, Calculator.divide(15, 3), 1e-6);
     }

    @Test
    public void testAbs(){
	assertEquals(0.5, Calculator.abs(-0.5), 1e-6);
     }
    
    @Test
    public void testPower(){
	assertEquals(1, Calculator.power(2, 0), 1e-6);
     }
}
